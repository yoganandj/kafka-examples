package com.kafka.example.demo;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class ProducerWithCallbackDemo {
    public static void main(String[] args) {
        Logger log = LoggerFactory.getLogger(ProducerWithCallbackDemo.class);
        System.out.println("Hello ---");
        // Create Producer properties
        Properties properties = new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        // Create the Producer
        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);

        for (int i=0;i<10;i++){
            // create a Producer Record

            String topic ="test_delivery_pull";
            String value = "hello"+i+" from program";
//            String key = "id_"+Integer.toString(i);
            ProducerRecord<String, String> record = new ProducerRecord<String, String>(topic, value);
            //send data - asynchronous
            producer.send(record, new Callback() {
                @Override
                public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                    // executes every time a record is successfully sent or an exception is thrown
                    if(e==null){
                        log.info("Received new metedata \n"+
                                "Topic : "+recordMetadata.topic()+" \n"+
                                "Partition : "+recordMetadata.partition()+" \n"+
                                "Offset : "+recordMetadata.offset()+" \n"+
                                "Timestamp : "+recordMetadata.timestamp()
                        );

                    }else {
                        log.error("Error while producing ", e);
                    }
                }
            });
        }


        //flush data
        producer.flush();
        producer.close();
    }
}
