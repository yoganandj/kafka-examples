package com.example.kafka.exception;

public class UserKafkaException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public UserKafkaException(String message, Throwable cause) {
        super(message,cause);
    }

    public UserKafkaException(String message) {
        super(message);
    }

    public UserKafkaException(Throwable cause) {
        super(cause);
    }
}
