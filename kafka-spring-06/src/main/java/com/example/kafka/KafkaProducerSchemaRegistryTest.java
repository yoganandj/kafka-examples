package com.example.kafka;

import com.example.kafka.util.SingletonHelper;
import com.example.kafka.util.SingletonSchemaRegistryHelper;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecordBuilder;
import org.apache.kafka.clients.producer.ProducerConfig;

import java.util.HashMap;
import java.util.Map;

public class KafkaProducerSchemaRegistryTest {
    private static final String bootStrapServers = "127.0.0.1:9092";
    private static final String schemaRegistryUrl = "http://127.0.0.1:8081";
    private static final String topic = "customer-avro";

    static {
        Map<String, String> props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapServers);
        props.put("schema.registry.url", schemaRegistryUrl);
        SingletonSchemaRegistryHelper.initialize(props);
    }
    
    public static void main(String[] args) {

        Schema.Parser parser = new Schema.Parser();
        Schema schema = parser.parse("{\n" +
                "     \"type\": \"record\",\n" +
                "     \"namespace\": \"com.example\",\n" +
                "     \"name\": \"Customer\",\n" +
                "     \"fields\": [\n" +
                "       { \"name\": \"first_name\", \"type\": \"string\", \"doc\": \"First Name of Customer\" },\n" +
                "       { \"name\": \"last_name\", \"type\": \"string\", \"doc\": \"Last Name of Customer\" },\n" +
                "       { \"name\": \"age\", \"type\": \"int\", \"doc\": \"Age at the time of registration\" },\n" +
                "       { \"name\": \"height\", \"type\": \"float\", \"doc\": \"Height at the time of registration in cm\" },\n" +
                "       { \"name\": \"weight\", \"type\": \"float\", \"doc\": \"Weight at the time of registration in kg\" },\n" +
                "       { \"name\": \"automated_email\", \"type\": \"boolean\", \"default\": true, \"doc\": \"Field indicating if the user is enrolled in marketing emails\" }\n" +
                "     ]\n" +
                "}");

        GenericRecordBuilder customerBuilder = new GenericRecordBuilder(schema);
        customerBuilder.set("first_name","TESTDEMO2");
        customerBuilder.set("last_name", "H");
        customerBuilder.set("age", 22);
        customerBuilder.set("height", 155f);
        customerBuilder.set("weight", 77);
        customerBuilder.set("automated_email", false);
        GenericData.Record customer = customerBuilder.build();

        SingletonSchemaRegistryHelper.getKafkaTemplate().push(topic, null, customer);
    }
}
