package com.example.kafka.util;

import com.example.kafka.producer.UserKafkaTemplate;
import com.example.kafka.producer.factory.ProducerStringFactory;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.producer.ProducerConfig;

import java.util.Map;

public class SingletonHelper {
    private static SingletonHelper singletonHelper = null;
    private static UserKafkaTemplate<String, String> kafkaTemplate;

    private SingletonHelper(){

    }

    public static SingletonHelper getInstance(){
        if( singletonHelper == null ){
            throw new RuntimeException("Error occurred during creation of SingletonHelper instance");
        }
        return singletonHelper;
    }

    public static void initialize(Map<String, String> props){
        if(singletonHelper == null){
            synchronized (SingletonHelper.class){
                if (singletonHelper == null){
                    initializeKafkaTemplate(props);
                    SingletonHelper.singletonHelper = new SingletonHelper();
                }
            }
        }
    }

    private static void initializeKafkaTemplate(Map<String, String> props) {
        ProducerStringFactory producerStringFactory = new ProducerStringFactory(props.get(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG), props.get("schema.registry.url"));
        SingletonHelper.kafkaTemplate = producerStringFactory.template();
    }

    public static UserKafkaTemplate<String, String> getKafkaTemplate() {
        return kafkaTemplate;
    }
}
