package com.example.kafka.util;

import com.example.kafka.producer.UserKafkaTemplate;
import com.example.kafka.producer.factory.ProducerSchemaRegistryFactory;
import com.example.kafka.producer.factory.ProducerStringFactory;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.producer.ProducerConfig;

import java.util.Map;

public class SingletonSchemaRegistryHelper {
    private static SingletonSchemaRegistryHelper singletonHelper = null;
    private static UserKafkaTemplate<GenericRecord, GenericRecord> kafkaTemplate;

    private SingletonSchemaRegistryHelper(){

    }

    public static SingletonSchemaRegistryHelper getInstance(){
        if( singletonHelper == null ){
            throw new RuntimeException("Error occurred during creation of SingletonHelper instance");
        }
        return singletonHelper;
    }

    public static void initialize(Map<String, String> props){
        if(singletonHelper == null){
            synchronized (SingletonSchemaRegistryHelper.class){
                if (singletonHelper == null){
                    initializeKafkaTemplate(props);
                    SingletonSchemaRegistryHelper.singletonHelper = new SingletonSchemaRegistryHelper();
                }
            }
        }
    }

    private static void initializeKafkaTemplate(Map<String, String> props) {
        ProducerSchemaRegistryFactory producerStringFactory = new ProducerSchemaRegistryFactory(props.get(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG), props.get("schema.registry.url"));
        SingletonSchemaRegistryHelper.kafkaTemplate = producerStringFactory.template();
    }

    public static UserKafkaTemplate<GenericRecord, GenericRecord> getKafkaTemplate() {
        return kafkaTemplate;
    }
}
