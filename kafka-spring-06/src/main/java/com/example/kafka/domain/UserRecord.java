package com.example.kafka.domain;

public class UserRecord<K,V> {

    private K key;
    private V value;

    public UserRecord (K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }
}
