package com.example.kafka.producer;

import com.example.kafka.domain.UserRecord;
import com.example.kafka.exception.UserKafkaException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.KafkaException;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import java.util.List;
import java.util.Map;

public class UserKafkaTemplate<K,V> extends KafkaTemplate<K,V> {
    private static final Logger log = LoggerFactory.getLogger(UserKafkaTemplate.class);

    public UserKafkaTemplate(ProducerFactory<K, V> producerFactory) {
        super(producerFactory);
    }
    public void push(String topic, K key, V data){
        send(topic, key, data);
        try{
            flush();
        }catch (Exception e){
            log.error("Error while committing the record for topic "+ topic + " data: "+data, e);
            throw new UserKafkaException("Error while committing the record for topic "+ topic + " data: "+data, e);
        }
    }

    public void push(String topic, Map<K,V> bulkRecord){
        bulkRecord.entrySet().stream().forEach(entry -> send(topic, entry.getKey(), entry.getValue()));
        try{
            flush();
        }catch (Exception e){
            log.error("Error while committing the record for topic "+ topic , e);
            throw new UserKafkaException("Error while committing the bulk records for topic ", e);
        }
    }

    public void push(String topic, List<UserRecord<K, V>> bulkRecord){
        bulkRecord.stream().forEach(userRecord -> send(topic, userRecord.getKey(), userRecord.getValue()));
        try{
            flush();
        }catch (Exception e){
            log.error("Error while committing the record for topic "+ topic,e);
            throw new UserKafkaException("Error while committing the bulk user records for topic "+ topic, e);
        }
    }


}
