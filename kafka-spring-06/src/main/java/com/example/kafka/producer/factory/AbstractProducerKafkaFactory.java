package com.example.kafka.producer.factory;

import org.apache.kafka.common.serialization.Serializer;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.ProducerFactory;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractProducerKafkaFactory<K,V> {

    protected Map<String, Object> props = new HashMap<>();

    public AbstractProducerKafkaFactory(String bootStrapServers, String schemaServers) {
         this.props.put("schema.registry.url", schemaServers);
         this.props.put("bootstrap.servers", bootStrapServers);
//         this.props.put("batch.size", )
    }

    protected Map<String, Object> producerConfigs(Map<String, Object> props){

        return props;
    }

    protected ProducerFactory<K,V> producerFactory(Serializer avroKeySerializer, Serializer avroValueSerializer){
        return (ProducerFactory<K, V>) new DefaultKafkaProducerFactory<>(this.props, avroKeySerializer, avroValueSerializer);
    }

    protected ProducerFactory<K,V> producerFactory(){
        return (ProducerFactory<K, V>) new DefaultKafkaProducerFactory<>(this.props);
    }
}
