package com.example.kafka.producer.factory;

import com.example.kafka.producer.UserKafkaTemplate;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.HashMap;
import java.util.Map;

public class ProducerStringFactory extends AbstractProducerKafkaFactory<String, String> {

    public ProducerStringFactory(String bootStrapServers, String schemaServers) {
        super(bootStrapServers, schemaServers);
    }

    private Map<String, Object> producerConfigs(){
       Map<String, Object> props = new HashMap<>();
       props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
       props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
       producerConfigs(props);
       return props;
    }


    public UserKafkaTemplate<String,String> template(){
        producerConfigs();
        StringSerializer stringKeySerializer = new StringSerializer();
        stringKeySerializer.configure(this.props, true);

        StringSerializer stringValueSerializer = new StringSerializer();
        stringValueSerializer.configure(this.props, false);

        return new UserKafkaTemplate(producerFactory((Serializer) stringKeySerializer, (Serializer) stringValueSerializer) );
    }



}
