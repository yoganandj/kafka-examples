package com.example.kafka.producer.factory;

import com.example.kafka.producer.UserKafkaTemplate;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.HashMap;
import java.util.Map;

public class ProducerSchemaRegistryFactory extends AbstractProducerKafkaFactory<GenericRecord, GenericRecord> {

    public ProducerSchemaRegistryFactory(String bootStrapServers, String schemaServers) {
        super(bootStrapServers, schemaServers);
    }

    private Map<String, Object> producerConfigs(){
       Map<String, Object> props = new HashMap<>();
       props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);
       props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);
       producerConfigs(props);
       return props;
    }


    public UserKafkaTemplate<GenericRecord,GenericRecord> template(){
        producerConfigs();
        KafkaAvroSerializer avroKeySerializer = new KafkaAvroSerializer();
        avroKeySerializer.configure(this.props, true);

        KafkaAvroSerializer avroValueSerializer = new KafkaAvroSerializer();
        avroValueSerializer.configure(this.props, false);

        return new UserKafkaTemplate(producerFactory((Serializer) avroKeySerializer, (Serializer) avroValueSerializer) );
    }



}
