package com.example.kafka;

import com.example.kafka.producer.UserKafkaTemplate;
import com.example.kafka.producer.factory.ProducerStringFactory;
import com.example.kafka.util.SingletonHelper;
import org.apache.kafka.clients.producer.ProducerConfig;

import java.util.HashMap;
import java.util.Map;

public class KafkaProducerStringTest {
    private static final String bootStrapServers = "127.0.0.1:9092";
    private static final String schemaRegistryUrl = "http://127.0.0.1:8081";
    private static final String topic = "user_topic";

    static {
        Map<String, String> props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapServers);
        props.put("schema.registry.url", schemaRegistryUrl);
        SingletonHelper.initialize(props);
    }
    
    public static void main(String[] args) {

        SingletonHelper.getKafkaTemplate().push(topic, null, "Simple Producer String msg2 using Single helper1");
    }
}
