package com.example;

import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Arrays;
import java.util.Properties;

public class KafkaAvroConsumerV1 {
    public static void main(String[] args) {
        String groupId = "my-customer3-application";
        String topic = "customer-avro";

        Properties properties = new Properties();
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        properties.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        properties.setProperty("specific.avro.reader", "true");
        properties.setProperty("schema.registry.url", "http://127.0.0.1:8081");


        KafkaConsumer<String, Customer> consumer = new KafkaConsumer<String, Customer>(properties);


        // subscribe consumer to our topics
        consumer.subscribe(Arrays.asList(topic));
        // poll for new data
        while (true) {
            ConsumerRecords<String, Customer> records = consumer.poll(100);
            for (ConsumerRecord<String, Customer> record : records) {
                Customer customer = record.value();
                System.out.println("schema : "+customer.getSchema());
                System.out.println("key : " + record.key() + ", value :" + customer );
                System.out.println("partition : " + record.partition() + ", offset : " + record.offset());
            }
            consumer.commitSync();
        }
    }
}
